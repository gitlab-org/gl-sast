# gl-sast

**REPOSITORY HAS BEEN MOVED TO: https://gitlab.com/gitlab-org/security-products/sast**


To ensure backward compatibility for users having a `.gitlab-ci.yml` pointing to that container registry (to pull the `gl-sast` image) we are keeping this repo alive and we will continue to push latest images here too.

See https://gitlab.com/gitlab-org/gl-sast/issues/14 for more details.
